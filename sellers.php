<?php
/***************************************************************************
 *   copyright				: (C) Rudi Paret
 *   site					: http://www.farm-ring.com/
 ***************************************************************************/

/***************************************************************************
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version. Although none of the code may be
 *   sold. If you have been sold this script, get a refund.
 ***************************************************************************/

include 'common.php';

 $query = "SELECT id, nick, name, city, groups FROM " . $DBPrefix . "users WHERE suspended = 0";
$db->direct_query($query);

while ($row = $db->fetch())                                                  
{
	$template->assign_block_vars('sellers', array(
			'ID' => $row['id'],
			'NICK' => $row['nick'],
			'NAME' => $row['name'],
			'CITY' => $row['city'],
			'GROUPS' => $row['groups']
			));
}

include 'header.php';
$template->set_filenames(array(
		'body' => 'sellers.tpl'
		));
$template->display('body');
include 'footer.php';