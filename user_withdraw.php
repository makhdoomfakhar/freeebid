<?php
/***************************************************************************
 *   copyright				: (C) 2008 - 2016 WeBid
 *   site					: http://www.webidsupport.com/
 ***************************************************************************/

/***************************************************************************
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version. Although none of the code may be
 *   sold. If you have been sold this script, get a refund.
 ***************************************************************************/

include 'common.php';

// If user is not logged in redirect to login page
if (!$user->checkAuth())
{
    $_SESSION['LOGIN_MESSAGE'] = $MSG['5000'];
    $_SESSION['REDIRECT_AFTER_LOGIN'] = 'user_withdraw.php';
    header('location: user_login.php');
    exit;
}
// check if the user can access this page
$user->checkSuspended();

$NOW = time();
$NOWB = date('Ymd');
$user_message = 'hello';

if(isset($_POST['withdraw-submit'])){
    $uid = $_SESSION['WEBID_LOGGED_IN'];
    $query = "SELECT * FROM webid_users WHERE id='".$uid."'";
    $params = array();
    $params[] = array(':user_id', $uid, 'int');
    $db->query($query, $params);
    $user_gateways = array();
    $user = $db->fetch();

    $currency="USD";
    $uid = $user['id'];
    $balance = $user['balance'];
    $amount = $_POST['amount'];

    if($amount <= $balance) {

        $que = "SELECT SUM(wd_amount) FROM webid_withdrawl WHERE wd_user_id='" . $uid . "' AND wd_status='0'";
        $db->query($query);
        $user_gateways = array();
        $user = $db->fetch();

        print_r($user);;

        $pendingWithdrawl=$wd_row['pendingWithdrawl'];

        if ($balance >= ($pendingWithdrawl+$amount)) {

            $qr = "INSERT INTO webid_withdrawl (wd_user_id, wd_amount, wd_currency) VALUES ('" . $uid . "', '" . $amount . "', '" . $currency . "')";

            $params = array();
            $params[] = array(':user_id', $uid, 'int');
            $db->query($qr, $params);
            $_SESSION['msg']['success'] = "Your withdrawl initiated successfully. Pending admin approval.".$pendingWithdrawl.$query;
        }else{
            $_SESSION['msg']['error']="Your withdrawl request of amount ".$pendingWithdrawl." is under approval. You can request for withdrawl within ".($balance-$pendingWithdrawl)." Amount";
        }
    }
    else{
        $_SESSION['msg']['error']="Your current balance is less than the withdrawal amount.";
    }
}

include 'header.php';
$TMP_usmenutitle = "Withdraw Funds";
include INCLUDE_PATH . 'user_cp.php';
$template->set_filenames(array(
    'body' => 'user_withdrawl.tpl'
));
$template->display('body');
include 'footer.php';



